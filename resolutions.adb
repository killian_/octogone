package body resolutions is

   -----------------------
   -- estChiffreValable --
   -----------------------

   function estChiffreValable
     (g : in Type_Grille;
      v :    Integer;
      c :    Type_Coordonnee)
      return Boolean
   is
   begin
      if not caseVide(g, c) then
         raise CASE_NON_VIDE;
      end if;
      return(v>=0 and v<10);
   end estChiffreValable;

   -------------------------------
   -- obtenirSolutionsPossibles --
   -------------------------------

   function obtenirSolutionsPossibles
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Type_Ensemble
   is
      e_possible: Type_Ensemble;
      e_carre: Type_Ensemble;
      e_ligne: Type_Ensemble;
      e_colonne: Type_Ensemble;
   begin
      if not caseVide(g, c) then
         raise CASE_NON_VIDE;
      end if;
      e_possible:=construireEnsemble;
      e_carre:=obtenirChiffresDUnCarre(g,obtenirCarre(c));
      e_ligne:=obtenirChiffresDUneLigne(g,obtenirLigne(c));
      e_colonne:=obtenirChiffresDUneColonne(g,obtenirColonne(c));
      for i in 1..9 loop
         if (not appartientChiffre(e_carre, i)) and (not appartientChiffre(e_ligne,i)) and (not appartientChiffre(e_colonne,i)) then
            ajouterChiffre(e_possible,i);
         end if;
      end loop;
      return e_possible ;

   end obtenirSolutionsPossibles;

   ------------------------------------------
   -- rechercherSolutionUniqueDansEnsemble --
   ------------------------------------------

   function rechercherSolutionUniqueDansEnsemble
     (resultats : in Type_Ensemble)
      return Integer
   is
      i:Integer:=1;
   begin
      if nombreChiffres(resultats) > 1  then
         raise PLUS_DE_UN_CHIFFRE;
      end if;
      if nombreChiffres(resultats) = 0 then
         raise ENSEMBLE_VIDE;
      end if;
      while not appartientChiffre(resultats, i) loop
         i:=i+1;
      end loop;
      return i;
   end rechercherSolutionUniqueDansEnsemble;

   --------------------
   -- resoudreSudoku --
   --------------------

   procedure resoudreSudoku
     (g      : in out Type_Grille;
      trouve :    out Boolean)
   is
      e_possible:Type_Ensemble;
      c:Type_Coordonnee;

   begin
      trouve:=True;
      while (not estRemplie(g) = True) and trouve loop
         trouve:=False;
         for colonne in 1..9 loop
            for ligne in 1..9 loop
               c:=construireCoordonnees(ligne,colonne);
               if caseVide(g,c) then
                  e_possible:=obtenirSolutionsPossibles(g,c);
                  if nombreChiffres(e_possible)=1 then
                     fixerChiffre(g,c,rechercherSolutionUniqueDansEnsemble(e_possible));
                     trouve:=True;
                  end if;
               end if;
            end loop;
         end loop;
      end loop;




   end resoudreSudoku;

end resolutions;
