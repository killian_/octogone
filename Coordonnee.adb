package body Coordonnee is

   ---------------------------
   -- construireCoordonnees --
   ---------------------------

   function construireCoordonnees
     (ligne   : in Integer;
      colonne : in Integer)
      return Type_Coordonnee
   is
      c : Type_Coordonnee;
   begin
      c.ligne := ligne;
      c.colonne := colonne;
      return c;
   end construireCoordonnees;

   ------------------
   -- obtenirLigne --
   ------------------

   function obtenirLigne (c :in Type_Coordonnee) return Integer is
   begin
      return c.ligne;
   end obtenirLigne;

   --------------------
   -- obtenirColonne --
   --------------------

   function obtenirColonne (c :in Type_Coordonnee) return Integer is
   begin
      return c.colonne;
   end obtenirColonne;

   ------------------
   -- obtenirCarre --
   ------------------

   function obtenirCarre (c :in Type_Coordonnee) return Integer is
      --      carre : Integer;
      a : Integer;
      b : Integer;
   begin
--        if c.ligne <= 3 and c.colonne <= 3 then
--           carre := 1;
--        elsif c.ligne <=3 and 4 <= c.colonne <= 6 then
--           carre := 2;
--        elsif c.ligne <=3 and 7 <= c.colonne <= 9 then
--           carre := 3;
--        elsif 4 <= c.ligne <= 6 and c.colonne <= 3 then
--           carre := 4;
--        elsif 4 <= c.ligne <= 6 and 4 <= c.colonne <= 6 then
--           carre := 5;
--        elsif 4 <= c.ligne <= 6 and 7 <= c.colonne <= 9 then
--           carre := 6;
--        elsif 7 <= c.ligne <= 9 and c.colonne <= 3 then
--           carre := 7;
--        elsif 7 <= c.ligne <= 9 and 4 <= c.colonne <= 6 then
--           carre := 8;
--        else 7 <= c.ligne <= 9 and 7 <= c.colonne <= 9 then
--           carre := 9;
--        end if;
--         return carre;
         a:=(c.ligne-1)/3;
         b:=(c.colonne-1)/3;
         return 3*a+b+1;
   end obtenirCarre;

   ----------------------------
   -- obtenirCoordonneeCarre --
   ----------------------------

   function obtenirCoordonneeCarre
     (numCarre :in Integer)
      return Type_Coordonnee
   is
      c : Type_Coordonnee;
   begin
      if numCarre = 1 then
         c := construireCoordonnees(1,1);
      elsif numCarre = 2 then
         c := construireCoordonnees(1,4);
      elsif numCarre = 3 then
         c := construireCoordonnees(1,7);
      elsif numCarre = 4 then
         c := construireCoordonnees(4,1);
      elsif numCarre = 5 then
         c := construireCoordonnees(4,4);
      elsif numCarre = 6 then
         c := construireCoordonnees(4,7);
      elsif numCarre = 7 then
         c := construireCoordonnees(7,1);
      elsif numCarre = 8 then
         c := construireCoordonnees(7,4);
      else
         c := construireCoordonnees(7,7);
      end if;
      return c;
   end obtenirCoordonneeCarre;

end Coordonnee;
