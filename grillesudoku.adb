package body grilleSudoku is

   ----------------------
   -- construireGrille --
   ----------------------

   function construireGrille return Type_Grille is
   grille : Type_Grille;
   begin
      for ligne in 1..9 loop
         for colonne in 1..9 loop
            grille(ligne,colonne) := 0;
         end loop;
      end loop;
       return grille;
   end construireGrille;

   --------------
   -- caseVide --
   --------------

   function caseVide
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Boolean
   is
   begin
      return (g(obtenirLigne(c),obtenirColonne(c)) = 0);
   end caseVide;

   --------------------
   -- obtenirChiffre --
   --------------------

   function obtenirChiffre
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Integer
   is
   begin
      if caseVide(g,c) then
         raise OBTENIR_CHIFFRE_NUL;
      end if;
      return (g(obtenirLigne(c),obtenirColonne(c)));
   end obtenirChiffre;

   --------------------
   -- nombreChiffres --
   --------------------

   function nombreChiffres (g : in Type_Grille) return Integer is
   nb : Integer := 0;
   begin
      for ligne in 1..9 loop
         for colonne in 1..9 loop
            if g(ligne,colonne) /= 0 then
               nb := nb+1;
            end if;
         end loop;
      end loop;
      return nb;
   end nombreChiffres;

   ------------------
   -- fixerChiffre --
   ------------------
   procedure fixerChiffre (g : in out Type_Grille; c : in Type_Coordonnee; v : in Integer) is
           begin
           if not caseVide(g,c) then
              raise FIXER_CHIFFRE_NON_NUL;
           end if;
           g(obtenirLigne(c),obtenirColonne(c)):=v;
           end fixerChiffre;

   ---------------
   -- viderCase --
   ---------------

   procedure viderCase (g : in out Type_Grille; c : in Type_Coordonnee) is
   begin
      if caseVide(g,c) then
         raise VIDER_CASE_VIDE;
      end if;
      g(obtenirLigne(c),obtenirColonne(c)) := 0;
   end viderCase;

   ----------------
   -- estRemplie --
   ----------------

   function estRemplie (g : in Type_Grille) return Boolean is
   begin
      for ligne in 1..9 loop
         for colonne in 1..9 loop
            if g(ligne,colonne)=0 then
               return FALSE;
            end if;
         end loop;
      end loop;
      return TRUE;
   end estRemplie;

   ------------------------------
   -- obtenirChiffresDUneLigne --
   ------------------------------

   function obtenirChiffresDUneLigne (g : in Type_Grille; numLigne : in Integer) return Type_Ensemble is
      ens : Type_Ensemble;
      c : Type_Coordonnee;
   begin
      ens:=construireEnsemble;
      for i in 1..9 loop
         c := construireCoordonnees(numLigne,i);
         if not caseVide(g,c) then
            ajouterChiffre(ens,(obtenirChiffre(g,c)));
         end if;
      end loop;
      return ens;
   end obtenirChiffresDUneLigne;

   --------------------------------
   -- obtenirChiffresDUneColonne --
   --------------------------------

   function obtenirChiffresDUneColonne(g : in Type_Grille; numColonne : in Integer) return Type_Ensemble is
      ens : Type_Ensemble;
      c : Type_Coordonnee;
   begin
      ens:=construireEnsemble;
      for i in 1..9 loop
         c := construireCoordonnees(i,numColonne);
         if not caseVide(g,c) then
            ajouterChiffre(ens,(obtenirChiffre(g,c)));
         end if;
      end loop;
      return ens;
   end obtenirChiffresDUneColonne;

   -----------------------------
   -- obtenirChiffresDUnCarre --
   -----------------------------

   function obtenirChiffresDUnCarre(g : in Type_Grille; numCarre : in Integer) return Type_Ensemble is
      ens : Type_Ensemble;
      sommetCarre : Type_Coordonnee;
      c : Type_Coordonnee;
   begin
      ens:=construireEnsemble;
      sommetCarre := obtenirCoordonneeCarre(numCarre);
      for ligne in (obtenirLigne(sommetCarre))..(obtenirLigne(sommetCarre)+2) loop
         for colonne in (obtenirColonne(sommetCarre))..(obtenirColonne(sommetCarre)+2) loop
            c := construireCoordonnees(ligne,colonne);
            if not caseVide(g,c) then
            ajouterChiffre(ens,(obtenirChiffre(g,c)));
            end if;
         end loop;
      end loop;
      return ens;
   end obtenirChiffresDUnCarre;

end grilleSudoku;
